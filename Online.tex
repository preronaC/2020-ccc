\documentclass[dvipsnames,aspectratio=169]{beamer}
\usetheme{focus}

\usepackage[english] {babel}
\input{macros}
\usepackage{amsmath,amssymb,amsfonts,graphicx}
\usepackage{commons}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usetikzlibrary{matrix}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing}

\newcommand{\zero}{\mathrm{0}}
\renewcommand{\phi}{\varphi}

\title{A Quadratic Lower Bound against \\ Algebraic Branching Programs}
\author
  {Prerona Chatterjee
  \\ \small{Tata Instutute of Fundamental Research, Mumbai}}

\date{July 29, 2020}
\institute{with Mrinal Kumar (\small{IITB}), Adrian She (\small{UoT}), Ben Lee Volk (\small{Caltech})}

\begin{document}
\tikzset{%
	block/.style    = {draw, thick, rectangle, minimum height = 3em,
		minimum width = 2em},
	sum/.style      = {draw, circle, node distance = 2cm}, % Adder
	input/.style    = {coordinate}, % Input
	output/.style   = {coordinate} % Output
}
\newcommand{\suma}{\Large$+$}
\newcommand{\proda}{\Large$\times$}

\maketitle

%\section{Algebraic Branching Programs}

\begin{frame}{Algebraic Branching Programs}
	\begin{tikzpicture}[thick, node distance=2cm]
		\node[circle, draw=black] (start) {$s$};
		\node[circle, draw=black] (l11) at ($(start)+(2.6,1.5)$) {};
		\node[circle, draw=black] (l12) at ($(start)+(2.6,-.5)$) {};
		\node[circle, draw=black] (l21) at ($(start)+(5.2,0)$) {};
		\node[circle, draw=black] (l31) at ($(start)+(7.8,1.5)$) {};
		\node[circle, draw=black] (l32) at ($(start)+(7.8,-.5)$) {};
		\node[circle, draw=black] (l41) at ($(start)+(10.4,1.5)$) {};
		\node[circle, draw=black] (l42) at ($(start)+(10.4,-.5)$) {};
		\node[circle, draw=black] (end) at ($(start)+(13,1)$) {t};
		
		\draw[->](start) -- (l11);
		\only<2-4>{\draw[->](start) -- node[left] {\small{$(2x+3)$}} (l11)};
		\only<3>{\textcolor{red}{\draw[->](start) -- node[left] {\small{\textcolor{black}{$(2x+3)$}}} (l11);}}
		\draw[->](start) -- (l12);
		\draw[->](l11) -- (l21);
		\only<2-4>{\draw[->](l11) -- node[left] {\small{$(x+3y)$}} (l21)};
		\only<3>{\textcolor{red}{\draw[->](l11) -- node[left] {\small{\textcolor{black}{$(x+3y)$}}} (l21);}}
		\draw[->](l12) -- (l21);
		\draw[->](l21) -- (l31);
		\only<2-4>{\draw[->](l21) -- node[left] {\small{$(y+5)$}} (l31)};
		\only<3>{\textcolor{red}{\draw[->](l21) -- node[left] {\small{\textcolor{black}{$(y+5)$}}}(l31);}}
		\draw[->](l21) -- (l32);
		\draw[->](l31) -- (l42);
		\only<2,4>{\draw[->](l31) -- node[left] {\small{$10$}} (l42)};
		\only<3>{\textcolor{red}{\draw[->](l31) -- node[left] {\small{\textcolor{black}{$10$}}}(l42);}}
		\draw[->](l32) -- (l41);
		\draw[->](l32) -- (l42);
		\draw[->](l41) -- (end);
		\draw[->](l42) -- (end);
		\only<2-4>{\draw[->](l42) -- node[right] {\small{$(x+y+7)$}} (end)};
		\only<3>{\textcolor{red}{\draw[->](l42) -- node[right] {\small{\textcolor{black}{$(x+y+7)$}}} (end);}}
	\end{tikzpicture}
	\vspace{1em} \pause
	\begin{itemize}
		\item Label on each edge: \hspace{1em} An affine linear form in $\set{x_1, x_2, \ldots , x_n}$ \pause
		\item Weight of path $p$ = $\mathsf{wt}(p)$: \hspace{1em} Product of the edge labels on $p$ \pause
		\item Polynomial computed by the ABP: \hspace{1em} $\sum_p \mathsf{wt}(p)$ \pause
	\end{itemize}
	
	\vspace{1em}
	
	\begin{center}
		\textbf{Central Question}: Can one give an $n$-variate, degree $d$ polynomial that can not be represented by an ABP of size $\poly(n, d)$?
	\end{center}
\end{frame}

\begin{frame}{Our Main Result}
	\textbf{General ABPs} [Baur-Strassen]:\\
	Any ABP computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(n \log d)$ \only<1-2>{wires}\only<3>{\textcolor{BrickRed}{wires}}.\\ \pause
	
	\vspace{2em}
	
	\textbf{Restricted ABPs} [Kumar]:\\
	Any ABP with $(d+1)$ layers computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(nd)$ vertices.\\ \pause
	
	\vspace{2em}
	
	\textbf{Our Main Result}:\\
		Any ABP computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(nd)$ \textcolor{BrickRed}{vertices}.
\end{frame}


\begin{frame}{Algebraic Formulas and a Lower Bound against them}
	\begin{columns}
		\begin{column}{.4\textwidth}
			\[
				x^3 + 3x^2y + 3xy^2 + y^3 = (x+y)^3
			\]

			\vspace{-1em}
			\[
				\text{\textcolor{MidnightBlue}{$(x+y) \cdot (x+y) \cdot (x+y)$}}
			\]

			\begin{center}
				\begin{tikzpicture}[thick, node distance=1cm]
					\draw
					node [name=dummy] {}
					node [sum, right of=dummy] (top) {\proda}
					node [sum] at ($(top)+(-1.5,-1.5)$) (sum1) {\suma}
					node [sum] at ($(top)+(0,-1.5)$) (sum2) {\suma}
					node [sum] at ($(top)+(1.5,-1.5)$) (sum3) {\suma};
					\node (var1) at ($(top)+(-1.8,-3)$) {$x$};
					\node (var2) at ($(top)+(-1.2,-3)$) {$y$};
					\node (var3) at ($(top)+(-0.3,-3)$) {$x$};
					\node (var4) at ($(top)+(0.3,-3)$) {$y$};
					\node (var5) at ($(top)+(1.2,-3)$) {$x$};
					\node (var6) at ($(top)+(1.8,-3)$) {$y$};
					
					\draw[->](sum1) -- (top);
					\draw[->](sum2) -- (top);
					\draw[->](sum3) -- (top);

					\draw[->](var1) -- (sum1);
					\draw[->](var2) -- (sum1);
					\draw[->](var3) -- (sum2);
					\draw[->](var4) -- (sum2);
					\draw[->](var5) -- (sum3);
					\draw[->](var6) -- (sum3);
				\end{tikzpicture}
			\end{center}
		\end{column} \pause

		\begin{column}{.6\textwidth}
			\textbf{[Kalorkoti]}: Any formula computing $\Det_{n\times n}$ requires $\Omega(n^3)$ \textcolor{BrickRed}{wires}.\\ \pause
			\vspace{.5em}
			\textbf{[Shpilka, Yehudayoff]} (using Kalorkoti's method):\\
			There is a multilinear polynomial such that any formula computing it requires $\Omega(n^2/\log n)$ \textcolor{BrickRed}{wires}.\\ \pause
			\vspace{1em}
			\textbf{Our Result}: Any formula computing $\mathsf{ESym}_{n,0.1n}$ requires $\Omega(n^2)$ \textcolor{BrickRed}{vertices}, where
			\[
				\mathsf{ESym}(n, 0.1n) = \sum_{i_1 < \cdots < i_{0.1n} \in [n]} \hspace{.5em} \prod_{j=1}^{0.1n} x_{i_j}.
			\]
		\end{column}
	\end{columns}	
\end{frame}

\begin{frame}{Other Results}
	\begin{enumerate}
		\item If the edge labels on the ABP are allowed to have degree $\Delta$, then the lower bound we get is $\Omega(n^2/\Delta)$.\\ $\text{ }$ \pause
		\item For unlayered ABPs with edge labels of degree $\leq \Delta$, the lower bound we get is $\Omega(n \log n/ \Delta \log \log n)$.\\ $\text{ }$ \pause
		\item The lower bound is also true for the elementary symmetric polynomial.
		\vspace{-.5em}
		\[
			\mathsf{ESym}(n, 0.1n) = \sum_{i_1 < \cdots < i_{0.1n} \in [n]} \hspace{.5em} \prod_{j=1}^{0.1n} x_{i_j}
		\]
	\end{enumerate}
\end{frame}

\begin{frame}{Open Threads}
	\begin{enumerate}
		\item Prove a quadratic lower bound (on wires?) for un-layered Algebraic Branching Programs. \pause
		\item Prove a super-quadratic lower bound on (homogeneous?) formulas (of constant depth?). \pause
		\item Reprove the $\Omega(n \log n)$ lower bound for general circuits. \pause
	\end{enumerate}
	\vspace{3em}
	\begin{center}
		\textbf{\large{Thank you!}}
	\end{center}
\end{frame}

\end{document}